import java.util.Random;
import java.util.Scanner;

/*
 * Rock Paper Scissors by alsagone
 */
public class Player
{
	int points ; 
	
	public Player()
	{
		this.points = 0 ; 
	}
	
	public static char item (String input)
	{
		char i ; 
		
		if (input.toLowerCase().startsWith("r"))
		{
			i = 'r' ; 
		}
		
		else if (input.toLowerCase().startsWith("p"))
		{
			i = 'p' ; 
		}
		
		else if (input.toLowerCase().startsWith("s"))
		{
			i = 's' ;
		}
		
		//Unknown item
		else
		{
			System.out.println("Invalid input : " + input);
			i = 'u' ; 
		}
		
		return i ; 
	}
	
	public static String displayItem (char item)
	{
		String s ; 
		switch (item)
		{
			case 'r' : s = "Rock" ; break ;
			case 'p' : s = "Paper" ; break ;
			case 's' : s = "Scissors" ; break ;
			default :  s = "Unknown item" ; break ;
		}
		
		return s ;
	}
	
	public static int fight (char item1, char item2)
	{
		int result ; 
		
		if ((item1 == item2) || (item2 == 'u'))
		{
			result = 0 ;
		}
		
		else
		{
			switch (item1)
			{
				case 'r' : if (item2 == 's') {result = 1 ;} else {result = -1 ;} break ;
				case 'p' : if (item2 == 'r') {result = 1 ;} else {result = -1 ;} break ;
				case 's' : if (item2 == 'p') {result = 1 ;} else {result = -1 ;} break ;
				default : result = 0 ; break ;
			}
		}
		
		return result ; 
	}
	
	//Returns a random number in [a;b]
	public static int myRand (int a, int b)
	{
		Random r = new Random() ;
		int min = Math.min(a,b) ;
		int max = Math.max(a,b) ;
		int n = r.nextInt(max-min+1) + min ;
		 
		return n ; 
	}
	
	//Randomly chooses an item
	public static char randomItemChoice()
	{
		String items = "rps" ; 
		int randomIndex = myRand(0,2) ; 
		return items.charAt(randomIndex) ;
	}
	
	public void play (int limit)
	{
		Player ai = new Player() ;
		char aiChoice, playerChoice ;
		String str ; 
		Scanner scan = new Scanner(System.in);
		int f ; 
		
		while ((this.points < limit) && (ai.points < limit))
		{
			System.out.println("You : " + this.points + "/" + limit) ; 
			System.out.println("The AI : " + ai.points + "/" + limit + "\n") ;
			
			aiChoice = randomItemChoice() ;
			
			System.out.print("Your choice : ") ;
			str = scan.nextLine() ;
			playerChoice = item(str) ;
			
			System.out.println("You choose " + displayItem(playerChoice) + ".") ;
			System.out.println("The AI chooses " + displayItem(aiChoice) + ".") ;
			f = fight(playerChoice, aiChoice) ;
			
			if (f == 1)
			{
				System.out.println("You win.");
				this.points++ ;
			}
			
			else if (f == 0)
			{
				System.out.println("Draw") ;
			}
			
			else
			{
				System.out.println("The AI wins.");
				ai.points++ ;
			}
			
			System.out.print("\n\n") ;
		}
		
		if (this.points == limit)
		{
			System.out.println("You win the game !");
		}
		
		else
		{
			System.out.println("The AI wins the game !");
		}
		
		scan.close();
		return ;
	}
	
	public static void main (String[] args)
	{
		Player p = new Player(); 
		p.play(10);
	}
}
